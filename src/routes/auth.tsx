import { RouteObject } from 'react-router-dom'
import Login from '@pages/Login'
import libMiddleware from '@arutek/core-app/libraries/middleware'
// import Home from '@src/pages/Home'

const routers:RouteObject[] = [
  {
    path: '/login',
    element: <Login />,
  },
  {
    path: '/logout',
    // element: <Home />,
    loader: async () => {
      return libMiddleware.logout('/login')
    }
  },
]

export default routers