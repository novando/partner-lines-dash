import { RouteObject } from 'react-router-dom'
import auth from './auth'
import manage from './manage'
import Dashboard from '@pages/Dashboard'
import Settings from '@pages/Settings'
import Home from '@pages/Home'
import Detail from '@pages/Detail'
import IncomeReport from '@pages/IncomeReport'
import Report from '../pages/Report'
import ReportId from '../pages/ReortId'
import SearchNopol from '../pages/SearchNopol'
import Maintenance from '@pages/Maintenance'
import libMiddleware from '@arutek/core-app/libraries/middleware'


const routers:RouteObject[] = [
  // { // Used only for maintenance
  //   path: '/:something?',
  //   element: <Maintenance />
  // },
  {
    path: '/',
    element: <Home />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login')
    },
  },
  {
    path: '/dashboard',
    element: <Dashboard />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login', 1)
    },
  },
  {
    path: '/settings',
    element: <Settings />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login', 1)
    },
  },
  {
    path: '/report',
    element: <Report />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login')
    },
  },
  {
    path: '/search',
    element: <SearchNopol />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login')
    },
  },
  {
    path: '/report/:id',
    element: <ReportId />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login')
    },
  },
  {
    path: '/:armadaId',
    element: <Detail />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login')
    },
  },

  {
    path: '/:income-report',
    element: <IncomeReport />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login')
    },
  },
  
  ...auth,
  ...manage,
]

export default routers