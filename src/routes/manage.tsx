import { RouteObject } from 'react-router-dom'
import libMiddleware from '@arutek/core-app/libraries/middleware'
import Po from '@src/pages/managements/Po'
import Tariff from '@src/pages/managements/Tariff'
import User from '@src/pages/managements/User'

const routers:RouteObject[] = [
  {
    path: '/manage-pos',
    element: <Po />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login', 1)
    }
  },
  {
    path: '/manage-tariffs',
    element: <Tariff />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login', 1)
    }
  },
  {
    path: '/manage-users',
    element: <User />,
    loader: async () => {
      return libMiddleware.isAuthenticated('/login', 1)
    }
  },
]

export default routers