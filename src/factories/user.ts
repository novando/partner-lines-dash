import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = import.meta.env.VITE_API_URL + '/user'

export default {
  async getUsers ():Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}`)
  },
  async getSelf ():Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}/self`)
  },
  async editSelf (payload:any):Promise<responseType> {
    return await libFetch.putDataLogged(`${apiUrl}`, payload)
  },
  async editKantor (userId:number, payload:userKantorReqDataType):Promise<responseType> {
    return await libFetch.putDataLogged(`${apiUrl}/kantor/${userId}`, payload)
  },
}