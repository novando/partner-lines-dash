import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = `${import.meta.env.VITE_API_URL}/kode-po`

export default {
  async getAll (query:kodePoFilterType):Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}`, query)
  },
  async add (payload:{idPo:string}):Promise<responseType> {
    return await libFetch.postDataLogged(`${apiUrl}`, payload)
  },
  async upload (formData:FormData):Promise<responseType> {
    return await libFetch.upDataLogged(`${apiUrl}/upload`, formData)
  },
}