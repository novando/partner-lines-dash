import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = `${import.meta.env.VITE_API_URL}/auth`

export default {
  async login (payload:loginType):Promise<responseType> {
    return await libFetch.postData(`${apiUrl}/login`, payload)
  },
  async register (payload:registerType):Promise<responseType> {
    return await libFetch.postDataLogged(`${apiUrl}/register`, payload)
  },
}