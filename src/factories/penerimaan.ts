import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = `${import.meta.env.VITE_API_URL}/penerimaan`

export default {
  async getAll (query?:PenerimaanQueryType):Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}`, query)
  },
  async getDetailById (id:number):Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}/${id}`)
  },
  async getDetailByNopol (nopol?:string):Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}/${nopol}`)
  },
}