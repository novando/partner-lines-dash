import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = `${import.meta.env.VITE_API_URL}/kantor`

export default {
  async getAll (query?:kantorQueryType):Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}`, query)
  },
}