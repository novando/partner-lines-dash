import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = `${import.meta.env.VITE_API_URL}/report`

export default {
  async getPo (query:reportQueryType):Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}/po`, query)
  },
  async getSummary (query:reportQueryType):Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}/summary`, query)
  },
}