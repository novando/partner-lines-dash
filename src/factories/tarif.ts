import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = `${import.meta.env.VITE_API_URL}/tarif`

export default {
  async getAll (query?:tarifFilterType):Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}`, query)
  },
  async add (payload:tarifReqType):Promise<responseType> {
    return await libFetch.postDataLogged(`${apiUrl}`, payload)
  },
  async del (kode:string):Promise<responseType> {
    return await libFetch.delDataLogged(`${apiUrl}/${kode}`)
  },
}