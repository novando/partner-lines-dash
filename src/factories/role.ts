import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = import.meta.env.VITE_API_URL

export default {
  async getRoles ():Promise<responseType> {
    return await libFetch.getDataLogged(`${apiUrl}/role`)
  },
}