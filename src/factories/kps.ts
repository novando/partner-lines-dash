import libFetch from '@arutek/core-app/libraries/fetch'

const apiUrl = import.meta.env.VITE_API_URL

export default {
  async uploadCsv (formData:FormData) {
    return await libFetch.upDataLogged(`${apiUrl}/kode-po/upload`, formData)
  },
  async getArmada (query?:armadaQueryType) {
    return await libFetch.getDataLogged(`${apiUrl}/armada`, query)
  },
  async dlArmada (query?:armadaQueryType, filename?:string) {
    return await libFetch.dlDataLogged(`${apiUrl}/armada/download`, filename, query)
  },
  async getArmadaDetails (armadaId:number) {
    return await libFetch.getDataLogged(`${apiUrl}/armada/${armadaId}`)
  },
  async generateArmada (query = {}) {
    return await libFetch.getDataLogged(`${apiUrl}/armada/generate`, query)
  },
  async getTarif (query:tarifFilterType = {}) {
    return await libFetch.getDataLogged(`${apiUrl}/tarif`, query)
  },
  async postKps (payload:kpsReqDataType):Promise<responseType> {
    return await libFetch.postDataLogged(`${apiUrl}/kps`, payload)
  },
  async upKps (payload:FormData):Promise<responseType> {
    return await libFetch.upDataLogged(`${apiUrl}/kps/upload`, payload)
  },
}