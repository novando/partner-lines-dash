
import { useEffect, useState } from 'react'
import helpCookie from '@arutek/core-app/helpers/cookie'
import Modal from '@arutek/core-app/components/Modal'
import imgMember1 from '@src/assets/images/20230713/whats-new-member-1.png'
import imgAdmin1 from '@src/assets/images/20230713/whats-new-admin-1.png'
import imgAdmin2 from '@src/assets/images/20230713/whats-new-admin-2.png'
import imgWarningLogo from '@arutek/core-app/images/common/warning-logo.svg'

const WhatsNew = () => {
  const theNews = [
    {
      img: imgWarningLogo,
      paragraphs: [
        `Tiap pengguna akan mempunyai wilayah tanggung jawab masing-masing yang dapat diatur oleh Admin, sehingga pengguna yang taggung jawab atas wilayah A, <span class="font-bold">TIDAK DAPAT</span> melihat data kendaraan dari wilayah B.`
      ],
      showedOnRole: [0,1,2],
    },
    {
      img: imgMember1,
      paragraphs: [
        `Akan muncul simbol baru di menu <span class="font-bold">Daftar Kendaraan</span> berupa <span class="font-bold">tanda seru berlatar belakang kuning</span> yang menandakan bahwa kendaraan terkait sudah memiliki tarif yang benar, tetapi <span class="font-bold">kapasitas tempat duduknya tidak valid</span>.`
      ],
      showedOnRole: [0,1,2],
    },
    {
      img: imgAdmin2,
      paragraphs: [
        `Data summary yang lebih komprehensif. (ADMIN ONLY)`
      ],
      showedOnRole: [0,1],
    },
    {
      img: imgAdmin1,
      paragraphs: [
        `Pengaturan kantor wilayah tiap user. Dapat di atur pada menu <span class="font-bold">Manajemen Pengguna</span> (ADMIN ONLY)`
      ],
      showedOnRole: [0,1],
    },
  ]

  const [page, setPage] = useState(0)
  const [news, setNews] = useState(theNews)
  const [isModal, setIsModal] = useState(false)

  useEffect(() => {
    loadComponent()
  }, [])
  
  const loadComponent = () => {
    const currVerSplit = (localStorage.getItem('appVersion') || '').split('.')
    const newVerSplit = helpCookie.getCookie('appVersion').split('.')
    const currMinVer = currVerSplit?.length > 1 ? `${currVerSplit[0]}.${currVerSplit[1]}` : ''
    const newMinVer = newVerSplit.length > 1 ? `${newVerSplit[0]}.${newVerSplit[1]}` : newVerSplit
    if (currMinVer === newMinVer) return
    let roleId:number
    try {
      roleId = JSON.parse(helpCookie.getCookie('userData')).roleId
    } catch {
      roleId = 99
    }
    setNews(theNews.filter((item) => {return item.showedOnRole.includes(roleId)}))
    setIsModal(true)
  }
  const nextpage = () => {
    page >= news.length -1 ? closeWhatsNew() : setPage(page + 1)
  }
  const closeWhatsNew = () => {
    localStorage.setItem('appVersion', helpCookie.getCookie('appVersion'))
    setIsModal(false)
  }
  return (
    <section>
      {isModal &&
        <Modal 
          title={`You are using a new version! (v${helpCookie.getCookie('appVersion')})`}
          modalBody={
            <section>
              <section className="flex flex-col gap-30 items-center">
                <img className="h-300 aspect-auto" src={news[page].img} alt="Info" />
                <div className="flex flex-col gap-16">
                  {news[page].paragraphs.map((text, key2) => (
                    <p key={key2} dangerouslySetInnerHTML={{__html: text}}></p>
                  ))
                  }
                </div>
                <div className="flex justify-between w-full">
                  <button type="button" className="btn">Tutup</button>
                  <button onClick={nextpage} type="button" className="btn btn-primary">{page >= news.length - 1 ? 'Selesai' : 'Berikutnya'}</button>
                </div>
              </section>
            </section>
          }
          closeModal={closeWhatsNew} />
      }
    </section>
  )
}

export default WhatsNew