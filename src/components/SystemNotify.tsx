import { useEffect, useState } from 'react'
import helpCookie from '@arutek/core-app/helpers/cookie'
import Modal from '@arutek/core-app/components/Modal'
import imgWarningLogo from '@arutek/core-app/images/common/warning-logo.svg'

const SystemNotify = () => {
  const [isModal, setIsModal] = useState(false)

  useEffect(() => {
    loadComponent()
  }, [])
  
  const loadComponent = () => {
    const currNotifVer = (localStorage.getItem('notifyVersion') || '')
    const newNotifVer = helpCookie.getCookie('notifyVersion')
    if (currNotifVer === newNotifVer) return
    else setIsModal(true)
  }
  const closeSystemNotify = () => {
    localStorage.setItem('notifyVersion', helpCookie.getCookie('notifyVersion'))
    setIsModal(false)
  }
  return (
    <section>
      {false &&
        <Modal 
          title="System Notification"
          modalBody={
            <section className="flex flex-col gap-30 items-center">
              <img className="wh120 aspect-auto" src={imgWarningLogo} alt="Alert" />
              <div className="flex flex-col gap-16">
                <p>Dalam rangka rencana proses pemutakhiran sistem pada hari <span className="font-bold">Rabu 12 Juli 2023 pukul 17.30 WIB</span> yang akan berlangsung <span className="font-bold">selama 4 jam lebih 30 menit</span>, maka diberitahukan kepada seluruh pengguna bahwa <span className="font-bold">selama proses tersebut aplikasi LINES tidak dapat diakses</span>.</p>
                <p>Aplikasi LINES dapat kembali digunakan pada hari <span className="font-bold">Rabu 12 Juli 2023 pukul 22.00 WIB</span>.</p>
                <p>Demikian pemberitahuan ini, kami mohon maaf atas ketidaknyamanan yang terjadi.</p>
              </div>
            </section>
          }
          closeModal={closeSystemNotify} />
      }
    </section>
  )
}

export default SystemNotify
