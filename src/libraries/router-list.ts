export default [
  {
    path: '/dashboard',
    name: 'Dashboard',
    accessLevel: 1,
  },
  {
    path: '/',
    name: 'Daftar Kendaraan'
  },
  {
    path: '/income-report',
    name: 'Lapor Penerimaan'
  },
  {
    path: '/report',
    name: 'Lapor'
  },
  {
    path: '/settings',
    name: 'Pengaturan',
    accessLevel: 1,
  },
  {
    path: '#',
    name: 'Manajemen',
    accessLevel: 1,
    child: [
      {
        path: '/manage-users',
        name: 'Pengguna',
        accessLevel: 1,
      },
      {
        path: '/manage-tariffs',
        name: 'Tarif',
        accessLevel: 1,
      },
      {
        path: '/manage-pos',
        name: 'PO',
        accessLevel: 1,
      },
    ],
  },
  {
    path: '/logout',
    name: 'Logout'
  },
]