import react from 'react'
import { useNavigate } from 'react-router-dom'
import DataTable from '@arutek/core-app/components/DataTable'
import WhatsNew from '@src/components/WhatsNew'
import Header from '@src/components/Header'
import notification from '@arutek/core-app/helpers/notification'
import imgJr from '@arutek/core-app/images/jasaraharja/logo.png'
import facKps from '@factories/kps'
import SystemNotify from '@src/components/SystemNotify'

const Home = () => {
  const navigate = useNavigate()
  const [isLoading, setIsLoading] = react.useState(false)
  const [isDownload, setIsDownload] = react.useState(false)
  const [armada, setArmada] = react.useState<armadaType[]>([])
  const [filter, setFilter] = react.useState<armadaQueryType>({})
  const [count, setCount] = react.useState(0)
  const [nopol, setNopol] = react.useState('')
  const [namaPo, setNamaPo] = react.useState('')
  const [kat, setKat] = react.useState(0)
  const [page, setPage] = react.useState(1)

  react.useEffect(() => {
    search(filter)
  }, [])

  const changedNopol = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNopol(e.target.value)
    search({nopol: e.target.value, page: 1})
  }
  const changedNamaPo = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNamaPo(e.target.value)
    search({namaPo: e.target.value, page: 1})
  }
  const changedKat = (e:react.ChangeEvent<HTMLSelectElement>) => {
    setKat(parseInt(e.target.value))
    search({kat: parseInt(e.target.value), page: 1})
  }
  const changedPageSize = (e:react.ChangeEvent<HTMLSelectElement>) => {
    search({size: parseInt(e.target.value)})
  }
  const nextPage = () => {
    if (isLoading) return
    setPage(page + 1)
    search({page: page + 1})
  }
  const prevPage = () => {
    if (isLoading) return
    setPage(page - 1)
    search({page: page - 1})
  }
  const search = async (val:armadaQueryType) => {
    setIsLoading(true)
    const newFilter = Object.assign(filter, val)
    setFilter(newFilter)
    try {
      const res = await facKps.getArmada(newFilter)
      setArmada(res.data)
      setCount(res.count)
    } catch (err:any) {
      notification.notifyError(err.toString())
    } finally {
      setIsLoading(false)
    }
  }
  const downloadCsv = async (val?:armadaQueryType) => {
    setIsDownload(true)
    const newFilter = Object.assign(filter, val)
    setFilter(newFilter)
    try {
      await facKps.dlArmada(newFilter, 'armada-list')
    } catch (err:any) {
      notification.notifyError(err.toString())
    } finally {
      setIsDownload(false)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <section className="text-center bg-white rounded-lg shadow p-20 mb-20">
          <img className="mx-auto w-160 my-20" src={imgJr} alt="Jasaraharja" />
          <h1 className="typ-title-regular-bold mb-8">LINES</h1>
          <p className="typ-title-small-medium">Licensing Identification & Notifications Electronic System</p>
        </section>
        <section className="bg-white rounded-lg shadow flex flex-wrap gap-20 p-20 mb-20">
          <div className="w-full">
            <div className="flex flex-row-reverse items-center gap-20 mb-16">
              <button onClick={() => downloadCsv()} className="btn btn-primary" type="button" disabled={isDownload}>{isDownload ? 'Downloading...' : 'Download CSV'}</button>
            </div>
            <div className="flex items-center gap-20">
              <div className="flex items-center gap-8">
                <p>Nopol</p>
                <input 
                  type="text"
                  className="input"
                  value={nopol}
                  placeholder='Cari Nopol'
                  onChange={changedNopol} />
              </div>
              <div className="flex items-center gap-8">
                <p>Nama PO</p>
                <input 
                  type="text"
                  className="input"
                  value={namaPo}
                  placeholder='Cari Nama PO'
                  onChange={changedNamaPo} />
              </div>
              <div className="flex items-center gap-8">
                <p>Kategori</p>
                <select value={kat} onChange={changedKat} className="input">
                  <option value={0}>Semua</option>
                  <option value={1}>1</option>
                  <option value={2}>2</option>
                  <option value={3}>3</option>
                </select>
              </div>
              <button onClick={() => search(filter)} className="btn btn-primary" type="button" disabled={isLoading}>Cari</button>
            </div>
          </div>
          <DataTable
            headComponent={
              <tr className="tr-head">
                <th className="py-8 w-20" />
                <th className="py-8">Nopol</th>
                <th className="py-8">Nama PO</th>
                <th className="text-center py-8">Kategori</th>
                <th className="py-8">Data DASI</th>
                <th className="py-8">Data Referensi</th>
              </tr>
            }
            bodyComponent={armada && armada.map((res, key) => (
              <tr key={key} onClick={() => navigate(`/${res.id}`)} className="tr-body">
                <td className="py-8">
                  {res.status === 'NOT_MATCHED' ? (
                    <div className="bg-red-70 rounded-full p-4 w-32 text-center text-white font-bold">!</div>
                  ) : res.status === 'SEAT_MISMATCH' ? (
                    <div className="bg-yellow-70 rounded-full p-4 w-32 text-center text-black font-bold">!</div>
                  ) : (
                    <div/>
                  )}
                </td>
                <td className="py-8">{res.nopol}</td>
                <td className="py-8">{res.poName}</td>
                <td className="text-center py-8">{res.poKat}</td>
                <td className="py-8">{res.dasi}</td>
                <td className="py-8">{res.reference}</td>
              </tr>
            ))}
            totalData={count}
            next={nextPage}
            prev={prevPage}
            size={changedPageSize}
            filter={filter} />
        </section>
        <SystemNotify />
        <WhatsNew />
      </main>
    </>
  )
}

export default Home
