import { useState, useEffect, ChangeEvent } from 'react'
import { useNavigate } from 'react-router-dom'
import DataTable from '@arutek/core-app/components/DataTable'
import Header from '@src/components/Header'
import WhatsNew from '@src/components/WhatsNew'
import notification from '@arutek/core-app/helpers/notification'
import imgJr from '@arutek/core-app/images/jasaraharja/logo.png'
import facPenerimaan from '@factories/penerimaan'
import SystemNotify from '@src/components/SystemNotify'
import libDate from '@arutek/core-app/libraries/date'
import helpFormatter from '@arutek/core-app/helpers/formatter'


const IncomeReport = () => {
  const navigate = useNavigate()
  
  const [isLoading, setIsLoading] = useState(false)
  const [filter, setFilter] = useState<armadaQueryType>({})
  const [iwkbuList, setIwkbuList] = useState<IwkbuDataType[]>([])
  const [iwkbuTotal, setIwkbuTotal] = useState(0)
  const [nopol, setNopol] = useState('')
  const [page, setPage] = useState(1)

  useEffect(() => {
    search(filter)
  }, [])
  
  const changedNopol = (e:ChangeEvent<HTMLSelectElement>) => {
    setNopol(e.target.value)
    search({nopol: e.target.value, page: 1})
  }
  const changedPageSize = (e:ChangeEvent<HTMLSelectElement>) => {
    search({size: parseInt(e.target.value)})
  }
  const nextPage = () => {
    if (isLoading) return
    setPage(page + 1)
    search({page: page + 1})
  }
  const prevPage = () => {
    if (isLoading) return
    setPage(page - 1)
    search({page: page - 1})
  }
  const search = async (val:armadaQueryType) => {
    setIsLoading(true)
    const newFilter = Object.assign(filter, val)
    setFilter(newFilter)
    try {
      const summary = await facPenerimaan.getAll(newFilter)
      setIwkbuList(summary.data)
      setIwkbuTotal(summary.count as number)
    } catch (err:any) {
      notification.notifyError(err.toString())
    } finally {
      setIsLoading(false)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <section className="text-center bg-white rounded-lg shadow p-20 mb-20">
          <img className="mx-auto w-160 my-20" src={imgJr} alt="Jasaraharja" />
          <h1 className="typ-title-regular-bold mb-8">LINES</h1>
          <p className="typ-title-small-medium">Licensing Identification & Notifications Electronic System</p>
        </section>
        <section className="bg-white rounded-lg shadow p-20 mb-20">
          <DataTable
            headComponent={
              <tr className="tr-head">
                <th className="py-8">Nopol</th>
                <th className="py-8">Tgl transaksi</th>
                <th className="py-8">SWDKLLJ</th>
                <th className="py-8">IWKBU</th>
                <th className="py-8">Nominal</th>
              </tr>
            }
            bodyComponent={iwkbuList && iwkbuList.map((res, key) => (
              <tr key={key} onClick={() => navigate(`/report/${res.id}`)} className="tr-body cursor-default">
                <td className="py-8">{res.nopol}</td>
                <td className="py-8">{libDate.isoToDate1(res.lastRecorded)}</td>
                <td className="py-8">{libDate.isoToDate1(res.lastSwdkllj)}</td>
                <td className="py-8">{libDate.isoToDate1(res.lastIwkbu)}</td>
                <td className="py-8">{helpFormatter.currency(res.lastPrice)}</td>
              </tr>
            ))}
            totalData={iwkbuTotal}
            next={nextPage}
            prev={prevPage}
            size={changedPageSize}
            filter={filter} />
        </section>
        <SystemNotify />
        <WhatsNew />
      </main>
    </>
  )
}

export default IncomeReport