import react from 'react'
import { useNavigate } from 'react-router-dom'
import pkg from '@root/package.json'
import LoginForm from '@arutek/core-app/components/auth/LoginForm'
import facAuth from '@factories/auth'
import facUser from '@factories/user'
import notify from '@arutek/core-app/helpers/notification'
import helpCookie from '@arutek/core-app/helpers/cookie'

const Login = () => {
  const navigate = useNavigate()
  const [isLoading, setIsLoading] = react.useState(false)
  const [credential, setCredential] = react.useState('')
  const [password, setPassword] = react.useState('')
  const getter = {password, credential}
  const setter = {setPassword, setCredential}

  react.useEffect(() => {
    helpCookie.setCookie('appVersion', pkg.version, 24 * 60)
  }, [])
  
  react.useEffect(() => {
    helpCookie.setCookie('notifyVersion', '1', 24 * 60)
  }, [])

  const login = async (e:react.FormEvent<HTMLFormElement>) => {
    setIsLoading(true)
    e.preventDefault()
    try {
      const payload = {credential, password}
      const res = await facAuth.login(payload)
      helpCookie.setAuthCookie(res.data, 30)
      const user = await facUser.getSelf()
      helpCookie.setCookie('userData', JSON.stringify(user.data), 30)
      notify.notifySuccess("Selamat datang")
      navigate('/', {replace: true})
    } catch (err:any) {
      notify.notifyError(err.message)
    } finally {
      setIsLoading(false)
    }
  }
  
  return (
    <main className="flex flex-col items-center justify-between bg-background h-full pt-80">
      <section className="bg-background flex flex-col gap-32 max-w-md mx-auto mb-60">
        <div className="text-center">
          <h1 className="text-4xl font-medium text-primary mb-8">LINES</h1>
          <h3 className="text-lg">Welcome, Sign in to your account</h3>
        </div>
        <form onSubmit={login} className="flex flex-col gap-24 bg-white border border-foreground rounded-lg p-24">
          <LoginForm getter={getter} setter={setter} />
          <button type="submit" className="btn btn-primary" disabled={isLoading}>Login</button>
        </form>
        <footer className="text-center py-16">
          <p>LINES v{pkg.version}</p>
          <p>&#169;2023 powered by <a href="https://arutek.com">Arutek</a></p>
        </footer>
      </section>
    </main>
  )
}

export default Login
