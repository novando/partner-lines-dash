import react from 'react'
import { useState } from 'react'
import DataTable from '@arutek/core-app/components/DataTable'
import Header from '@src/components/Header'
import WhatsNew from '@src/components/WhatsNew'
import notification from '@arutek/core-app/helpers/notification'
import imgJr from '@arutek/core-app/images/jasaraharja/logo.png'
import facReport from '@factories/report'
import SystemNotify from '@src/components/SystemNotify'

const SearchNopol = () => {
  const [isLoading, setIsLoading] = react.useState(false)
  const [filter, setFilter] = react.useState<armadaQueryType>({})
  const [total, setTotal] = react.useState(0)
  const [notMatch, setNotMatch] = react.useState(0)
  const [match, setMatch] = react.useState(0)
  const [overload, setOverload] = react.useState(0)
  const [dataEmpty, setDataEmpty] = react.useState(0)
  const [poList, setPoList] = react.useState<poResDataType[]>([])
  const [poTotal, setPoTotal] = react.useState(0)
  const [namaPo, setNamaPo] = react.useState('')
  const [kat, setKat] = react.useState(0)
  const [page, setPage] = react.useState(1)

  react.useEffect(() => {
    search(filter)
  }, [])
  
  const changedNamaPo = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNamaPo(e.target.value)
    search({namaPo: e.target.value, page: 1})
  }
  const changedKat = (e:react.ChangeEvent<HTMLSelectElement>) => {
    setKat(parseInt(e.target.value))
    search({kat: parseInt(e.target.value), page: 1})
  }
  const changedPageSize = (e:react.ChangeEvent<HTMLSelectElement>) => {
    search({size: parseInt(e.target.value)})
  }
  const nextPage = () => {
    if (isLoading) return
    setPage(page + 1)
    search({page: page + 1})
  }
  const prevPage = () => {
    if (isLoading) return
    setPage(page - 1)
    search({page: page - 1})
  }
  const [cariNopol, setCariNopol] = useState('')
  const search = async (val:armadaQueryType) => {
    setIsLoading(true)
    const newFilter = Object.assign(filter, val)
    setFilter(newFilter)
    try {
      const summary = await facReport.getSummary(newFilter)
      const po = await facReport.getPo(newFilter)
      const summaryData:summaryResDataType = summary.data
      setTotal(summaryData.total)
      setNotMatch(summaryData.notMatch)
      setMatch(summaryData.match)
      setOverload(summaryData.overload)
      setDataEmpty(summaryData.empty)
      setPoList(po.data)
      setPoTotal(po.count as number)
    } catch (err:any) {
      notification.notifyError(err.toString())
    } finally {
      setIsLoading(false)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <section className="text-center bg-white rounded-lg shadow p-20 mb-20">
          <img className="mx-auto w-160 my-20" src={imgJr} alt="Jasaraharja" />
          <h1 className="typ-title-regular-bold mb-8">LINES</h1>
          <p className="typ-title-small-medium">Licensing Identification & Notifications Electronic System</p>
        </section>
        <section className="text-center bg-white rounded-lg shadow p-20 mb-20"> 
          <p className='mb-20'>Masukkan Nopol (Tanpa strip)</p>
          <div className='mb-20'>
            <input type="text" onChange={setCariNopol} className="input border-solid" />
          </div>
          <button className="btn btn-primary mb-10">Cari Nopol</button>

        </section>
        

        <SystemNotify />
        <WhatsNew />
      </main>
    </>
  )
}

export default SearchNopol