import react from 'react'
import DataTable from '@arutek/core-app/components/DataTable'
import WhatsNew from '@src/components/WhatsNew'
import Modal from '@arutek/core-app/components/Modal'
import notification from '@arutek/core-app/helpers/notification'
import Header from '@src/components/Header'
import facTarif from '@src/factories/tarif'
import SystemNotify from '@src/components/SystemNotify'

const ManageUsers = () => {
  const izinList = ['AKAP', 'AKDP', 'ANGDES', 'ASK', 'PARIWISATA', 'TAKSI']
  const golList = ['Ekonomi', 'Non Ekonomi']
  const [tarifs, setTarifs] = react.useState<tarifType[]>([])
  const [filter, setFilter] = react.useState<tarifFilterType>({ page: 1, size: 10 })
  const [isLoading, setIsLoading] = react.useState(false)
  const [newWithGolongan, setNewWithGolongan] = react.useState(false)
  const [newUnlimitedSeat, setNewUnlimitedSeat] = react.useState(true)
  const [newKodeTarif, setNewKodeTarif] = react.useState('')
  const [newTarif, setNewTarif] = react.useState(0)
  const [newIzin, setNewIzin] = react.useState(izinList[0])
  const [newGol, setNewGol] = react.useState('')
  const [newMax, setNewMax] = react.useState(0)
  const [newMin, setNewMin] = react.useState(0)
  const [isDelModal, setIsDelModal] = react.useState(false)
  const [isNewTarifModal, setIsNewTarifModal] = react.useState(false)
  const [kode, setKode] = react.useState('')

  react.useEffect(() => {
    search(filter)
  }, [])
  
  const changeIzin = (e:react.ChangeEvent<HTMLInputElement>) => {
    search({izin: e.target.value, page: 1})
  }
  const changeGolongan = (e:react.ChangeEvent<HTMLInputElement>) => {
    search({golongan: e.target.value, page: 1})
  }
  const changeNewIzin = (val:react.ChangeEvent<HTMLSelectElement>) => {
    setNewIzin(val.target.value)
  }
  const changeNewGol = (val:react.ChangeEvent<HTMLSelectElement>) => {
    setNewGol(val.target.value)
  }
  const changeNewWithGolongan = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNewWithGolongan(e.target.checked)
    setNewGol(golList[0])
    if (!e.target.checked) setNewGol('')
  }
  const changeNewUnlimitedSeat = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNewUnlimitedSeat(e.target.checked)
    if (e.target.checked) setNewMax(-1)
  }
  const changeNewTarif = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNewTarif(parseInt(e.target.value))
  }
  const changeNewMax = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNewMax(parseInt(e.target.value))
  }
  const changeNewMin = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNewMin(parseInt(e.target.value))
  }
  const changeNewKodeTarif = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNewKodeTarif(e.target.value)
  }
  const deleteTarif = (kode:string) => {
    setIsDelModal(true)
    setKode(kode)
  }
  const confDelete = async () => {
    setIsLoading(true)
    try {
      await facTarif.del(kode)
      notification.notifySuccess(`Kode tarif ${kode} berhasil dihapus`)
      closeDelModal()
    } catch (err:any) {
      notification.notifyError(err.message)
    } finally {
      search(filter)
      setIsLoading(false)
    }
  }
  const createNewTarif = async (e:react.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setIsLoading(true)
    const payload:tarifReqType = {
      kodeTarif: newKodeTarif,
      tarif: newTarif,
      izin: newIzin,
      golongan: newGol,
      minSeat: newMin,
      maxSeat: newMax
    }
    try {
      await facTarif.add(payload)
      notification.notifySuccess(`Kode tarif ${newKodeTarif} berhasil ditambahkan`)
      closeNewTarifModal()
    } catch (err:any) {
      notification.notifyError(err.message)
    } finally {
      search(filter)
      setIsLoading(false)
    }
  }
  const closeDelModal = () => {
    setKode('')
    setIsDelModal(false)
  }
  const closeNewTarifModal = () => {
    setNewGol('')
    setNewIzin('')
    setNewKodeTarif('')
    setNewMin(0)
    setNewMax(-1)
    setNewTarif(0)
    setNewUnlimitedSeat(true)
    setNewWithGolongan(false)
    setIsNewTarifModal(false)
  }
  const search = async (val: tarifFilterType) => {
    const newFilter = Object.assign(filter, val)
    setFilter(newFilter)
    try {
      const res = await facTarif.getAll(newFilter)
      setTarifs(res.data)
    } catch (err:any) {
      notification.notifyError(err.message)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <section className="bg-white rounded-lg shadow p-20 mb-20">
          <div className="flex justify-between items-start gap-20 mb-16">
            <p className="text-xl font-bold mb-20">Daftar Tarif</p>
            <button onClick={() => setIsNewTarifModal(true)} className="btn btn-primary" type="button">Tambah Kode Tarif</button>
          </div>
          <div className="flex justify-between w-full">
            <div className="flex items-center gap-20">
              <div className="flex items-center gap-8">
                <p>Izin</p>
                <input
                  type="text"
                  className="input"
                  placeholder='Cari Izin'
                  onChange={changeIzin} />
              </div>
              <div className="flex items-center gap-8">
                <p>Golongan</p>
                <input
                  type="text"
                  className="input"
                  placeholder='Cari Golongan'
                  onChange={changeGolongan} />
              </div>
              <button onClick={() => search(filter)} className="btn btn-primary" type="button">Cari</button>
            </div>
          </div>
          <DataTable
            headComponent={
              <tr className="tr-head">
                <th className="p-8">No</th>
                <th className="p-8">Kode Tarif</th>
                <th className="p-8">Izin</th>
                <th className="p-8">Golongan</th>
                <th className="text-center p-8">Min Seat</th>
                <th className="text-center p-8">Max Seat</th>
                <th className="p-8">Tarif</th>
                <th className="p-8">Tindakan</th>
              </tr>
            }
            bodyComponent={tarifs && tarifs.map((res, key) => (
              <tr key={key} className="tr-body cursor-default">
                <td className="p-8">{key + 1}</td>
                <td className="p-8">{res.kodeTarif}</td>
                <td className="p-8">{res.izin}</td>
                <td className="p-8">{res.golongan.length > 0 ? res.golongan : '-'}</td>
                <td className="text-center p-8">{res.minSeat}</td>
                <td className="text-center p-8">{res.maxSeat < 1 ? 'Tak terbatas' : res.maxSeat}</td>
                <td className="text-end p-8">{res.tarif}</td>
                <td className="p-8">
                  <button onClick={() => deleteTarif(res.kodeTarif)} className="btn-square btn-danger-invert" type="button"><i className="aru-icon-trash" /></button>  
                </td>
              </tr>
            ))}/>
        </section>
        {isDelModal && 
          <Modal 
            title={`Hapus Kode tarif ${kode}`}
            modalBody={
              <section>
                <p className="mb-32">Apakah anda yakin akan menghapus Kode Tarif <span className="font-semibold">{kode}</span> dari daftar tarif?</p>
                <div className="bg-danger text-background rounded p-16 mb-60">
                  <p className="font-bold text-xl">PERHATIAN!</p>
                  <p>Semua kendaraan yang menggunakan kode tarif {kode}, akan kehilangan informasi tarifnya.</p>
                </div>
                <div className="flex justify-evenly">
                  <button onClick={confDelete} type="button" className="btn btn-danger" disabled={isLoading}>{isLoading ? 'Processing...' : 'Hapus'}</button>
                  <button onClick={closeDelModal} type="button" className="btn">Batal</button>
                </div>
              </section>
            }
            closeModal={closeDelModal} />
        }
        {isNewTarifModal && 
          <Modal 
            title={`Hapus Kode tarif ${kode}`}
            modalBody={
              <form onSubmit={createNewTarif} className="flex flex-col gap-24">
                <label className="flex justify-between">
                  <span>Kode Tarif</span>
                  <input className="input" onChange={changeNewKodeTarif} type="text" required />
                </label>
                <label className="flex justify-between">
                  <span>Izin</span>
                  <select className="input" value={newIzin} onChange={(e) => changeNewIzin(e)}>
                    {izinList.map((item, key) => (
                      <option key={key} value={item}>{item}</option>
                    ))}
                  </select>
                </label>
                <label>
                  <input type="checkbox" onChange={changeNewWithGolongan} />
                  <span>Ada golongan</span>
                </label>
                {newWithGolongan && (
                  <label className="flex justify-between">
                    <span>Golongan</span>
                    <select className="input" value={newGol} onChange={(e) => changeNewGol(e)}>
                      {golList.map((item, key) => (
                        <option key={key} value={item}>{item}</option>
                      ))}
                    </select>
                  </label>
                )}
                <label className="flex justify-between">
                  <span>Tarif</span>
                  <input className="input" onChange={changeNewTarif} type="number" required />
                </label>
                <label className="flex justify-between">
                  <span>Minimum Seat</span>
                  <input className="input" onChange={changeNewMin} type="number" min="0" required />
                </label>
                <label>
                  <input type="checkbox" onChange={changeNewUnlimitedSeat} checked={newUnlimitedSeat} />
                  <span>Maximum Seat unlimited?</span>
                </label>
                {!newUnlimitedSeat && (
                  <label className="flex justify-between">
                    <span>Maximum Seat</span>
                    <input className="input" onChange={changeNewMax} type="number" min="1" required />
                  </label>
                )}
                <button className="btn btn-primary" type="submit" disabled={isLoading}>{isLoading ? 'Processing...' : 'Buat Tarif'}</button>
              </form>
            }
            closeModal={closeNewTarifModal} />
        }
        <SystemNotify />
        <WhatsNew />
      </main>
    </>
  )
}

export default ManageUsers