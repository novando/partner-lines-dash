import react from 'react'
import DataTable from '@arutek/core-app/components/DataTable'
import Modal from '@arutek/core-app/components/Modal'
import notification from '@arutek/core-app/helpers/notification'
import Header from '@src/components/Header'
import facUser from '@src/factories/user'
import facAuth from '@src/factories/auth'
import facRole from '@src/factories/role'
import SystemNotify from '@src/components/SystemNotify'
import facKantor from '@src/factories/kantor'

const ManageUsers = () => {
  const [users, setUsers] = react.useState<userResDataType[]>([])
  const [kantors, setKantors] = react.useState<kantorResDataType[]>([])
  const [roles, setRoles] = react.useState<roleResDataType[]>([])
  const [filter, setFilter] = react.useState<userFilterType>({ page: 1, size: 10 })
  const [username, setUsername] = react.useState('')
  const [newUser, setNewUser] = react.useState('')
  const [newRole, setNewRole] = react.useState(0)
  const [newKantor, setNewKantor] = react.useState(0)
  const [newRoleName, setNewRoleName] = react.useState('')
  const [newPass, setNewPass] = react.useState('')
  const [newConf, setNewConf] = react.useState('')
  const [isLoading, setIsLoading] = react.useState(false)
  const [isModal, setIsModal] = react.useState(false)

  react.useEffect(() => {
    init()
  }, [])

  const init = async () => {
    const resKantors = await facKantor.getAll()
    const resRoles = await facRole.getRoles()
    const resUsers = await facUser.getUsers()
    const resKantorData:kantorResDataType[] = resKantors.data
    setKantors(resKantorData)
    setRoles(resRoles.data)
    setUsers(resUsers.data)
    setNewRole(resRoles.data[0].id)
    setNewRoleName(resRoles.data[0].name)
  }
  const changeUsername = (e:react.ChangeEvent<HTMLInputElement>) => {
    const val = e.target.value
    setUsername(val)
    setFilter(Object.assign(filter, { username: val }))
  }
  const changeNewUser = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNewUser(e.target.value)
  }
  const changeNewPass = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNewPass(e.target.value)
  }
  const changeNewConf = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNewConf(e.target.value)
  }
  const changeNewRole = (val:roleResDataType) => {
    setNewRole(val.id)
    setNewRoleName(val.name)
  }
  const changedKantorId = async (e:react.ChangeEvent<HTMLSelectElement>, userId:number) => {
    const kantorId = parseInt(e.target.value)
    try {
      const res = await facUser.editKantor(userId, {kantorId: kantorId})
      const newUsers = users.map((item) => {
        if (item.id === userId) item.kantorId = kantorId
        return item
      })
      setUsers(newUsers)
      notification.notifySuccess(res.message)
    } catch (err:any) {
      notification.notifyError(err.message)
    }
  }
  const changeNewKantor = (e:react.ChangeEvent<HTMLSelectElement>) => {
    setNewKantor(parseInt(e.target.value))
  }
  const search = (val:userFilterType) => {
    return val
  }
  const getRoleName = (id:number) => {
    const res = roles.find((item) => item.id === id )
    return res?.name || id
  }
  const createUser = async (e:react.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (newUser === '') return notification.notifyWarn('"Username" harus diisi!')
    if (newPass === '') return notification.notifyWarn('"Password" harus diisi!')
    if (newConf === '') return notification.notifyWarn('"Konfirmasi Password" harus diisi!')
    if (newConf !== newPass) {
      setNewConf('')
      setNewPass('')
      return notification.notifyWarn('Password tidak cocok, silahkan isi ulang!')
    }
    setIsLoading(true)
    try {
      const payload = {
        username: newUser,
        password: newPass,
        passConf: newConf,
        roleId: newRole,
        kantorId: newKantor,
      }
      await facAuth.register(payload)
      notification.notifySuccess(`User ${newUser} berhasil didaftarkan sebagai ${newRoleName}`)
    } catch (err: any) {
      notification.notifyError(err.message)
    } finally {
      setIsLoading(false)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <section className="bg-white rounded-lg shadow p-20 mb-20">
          <div>
            <p className="text-xl font-bold mb-20">Daftar Pengguna</p>
          </div>
          <div className="flex justify-between w-full">
            <div className="flex items-center gap-20">
              <div className="flex items-center gap-8">
                <p>Username</p>
                <input
                  type="text"
                  className="input"
                  value={username}
                  placeholder='Cari Username'
                  onChange={changeUsername} />
              </div>
              <button onClick={() => search(filter)} className="btn btn-primary" type="button">Cari</button>
            </div>
            <button onClick={() => setIsModal(true)} className="btn btn-primary">Buat Pengguna Baru</button>
          </div>
          <DataTable
            headComponent={
              <tr className="tr-head">
                <th className="py-8">No</th>
                <th className="py-8">Username</th>
                <th className="py-8">Kantor</th>
                <th className="text-center py-8">Hak Akses</th>
              </tr>
            }
            bodyComponent={users && users.map((res, key) => (
              <tr key={key} className="tr-body">
                <td className="py-8">{key + 1}</td>
                <td className="py-8">{res.username}</td>
                <td className="py-8">
                  <select className="input-sm" value={res.kantorId} onChange={(e) => changedKantorId(e, res.id)}>
                    {kantors.map((item, key) => (
                      <option key={key} value={item.id}>{item.name}</option>
                    ))}
                  </select>  
                </td>
                <td className="text-center py-8">{getRoleName(res.roleId)}</td>
              </tr>
            ))} />
        </section>
        {isModal && 
          <Modal 
            title="Buat Pengguna Baru"
            modalBody={
              <form onSubmit={createUser} className="flex flex-col gap-24">
                <label className="flex justify-between">
                  <span>Username</span>
                  <input className="input" onChange={changeNewUser} type="text" />
                </label>
                <label className="flex justify-between">
                  <span>Hak Akses</span>
                  <select className="input" value={newRoleName}>
                    {roles.map((item) => (
                      <option onClick={() => changeNewRole(item)} key={item.id} value={item.name}>{item.name}</option>
                    ))}
                  </select>
                </label>
                <label className="flex justify-between">
                  <span>Kantor</span>
                  <select className="input" value={newKantor} onChange={changeNewKantor}>
                    {kantors.map((item) => (
                      <option key={item.id} value={item.id}>{item.name}</option>
                    ))}
                  </select>
                </label>
                <label className="flex justify-between">
                  <span>Password</span>
                  <input className="input" onChange={changeNewPass} type="password" />
                </label>
                <label className="flex justify-between">
                  <span>Konfirmasi Password</span>
                  <input className="input" onChange={changeNewConf} type="password" />
                </label>
                <button className="btn btn-primary" type="submit" disabled={isLoading}>Buat User</button>
              </form>
            }
            closeModal={() => setIsModal(false)} />
        }
        <SystemNotify />
      </main>
    </>
  )
}

export default ManageUsers