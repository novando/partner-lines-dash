import react from 'react'
import WhatsNew from '@src/components/WhatsNew'
import DataTable from '@arutek/core-app/components/DataTable'
import notification from '@arutek/core-app/helpers/notification'
import Header from '@src/components/Header'
import facKodePo from '@src/factories/kode-po'
import SystemNotify from '@src/components/SystemNotify'

const ManageUsers = () => {
  const [pos, setPos] = react.useState<kodePoResTpe[]>([])
  const [count, setCount] = react.useState(0)
  const [filter, setFilter] = react.useState<kodePoFilterType>({ page: 1, size: 10 })

  react.useEffect(() => {
    search(filter)
  }, [])
  
  const next = () => {
    search({page: filter.page as number + 1})
  }
  const prev = () => {
    search({page: filter.page as number - 1})
  }
  const changeSize = (e:react.ChangeEvent<HTMLSelectElement>) => {
    search({size: parseInt(e.target.value)})
  } 
  const changeKode = (e:react.ChangeEvent<HTMLInputElement>) => {
    search({kodePo: e.target.value, page: 1})
  }
  const changeName = (e:react.ChangeEvent<HTMLInputElement>) => {
    search({namaPo: e.target.value, page: 1})
  }
  const search = async (val: kodePoFilterType) => {
    const newFilter = Object.assign(filter, val)
    setFilter(newFilter)
    try {
      const res = await facKodePo.getAll(newFilter)
      setPos(res.data)
      setCount(res.count as number)
    } catch (err:any) {
      notification.notifyError(err.message)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <section className="bg-white rounded-lg shadow p-20 mb-20">
          <div>
            <p className="text-xl font-bold mb-20">Daftar PO</p>
          </div>
          <div className="flex justify-between w-full">
            <div className="flex items-center gap-20">
              <div className="flex items-center gap-8">
                <p>Kode PO</p>
                <input
                  type="text"
                  className="input"
                  placeholder='Cari Kode PO'
                  onChange={changeKode} />
              </div>
              <div className="flex items-center gap-8">
                <p>Nama PO</p>
                <input
                  type="text"
                  className="input"
                  placeholder='Cari Nama PO'
                  onChange={changeName} />
              </div>
              <button onClick={() => search(filter)} className="btn btn-primary" type="button">Cari</button>
            </div>
          </div>
          <DataTable
            headComponent={
              <tr className="tr-head">
                <th className="py-8">No</th>
                <th className="py-8">Kode Po</th>
                <th className="text-center py-8">Nama PO</th>
              </tr>
            }
            bodyComponent={pos && pos.map((res, key) => (
              <tr key={key} className="tr-body">
                <td className="py-8">{key + 1}</td>
                <td className="py-8">{res.idPo}</td>
                <td className="text-center py-8">{res.name}</td>
              </tr>
            ))}
            filter={filter}
            totalData={count}
            next={next}
            prev={prev}
            size={changeSize} />
        </section>
        <SystemNotify />
        <WhatsNew />
      </main>
    </>
  )
}

export default ManageUsers