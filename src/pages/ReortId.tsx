import react from 'react'
import { useParams, Link } from 'react-router-dom'
import WhatsNew from '@src/components/WhatsNew'
import Header from '@src/components/Header'
import notification from '@arutek/core-app/helpers/notification'
import libDate from '@arutek/core-app/libraries/date'
import helpFormatter from '@arutek/core-app/helpers/formatter'
import facPenerimaan from '@factories/penerimaan'
import SystemNotify from '@src/components/SystemNotify'

const ReportId = () => {
  const { id } = useParams()
  const [isEdit, setIsEdit] = react.useState(false)
  const [nopol, setNopol] = react.useState('')
  const [lastIwkbu, setLastIwkbu] = react.useState('-')
  const [lastPrice, setLastPrice] = react.useState(0)
  const [lastSdwkllj, setLastSdwkllj] = react.useState('-')
  const [lastReceipt, setLastReceipt] = react.useState('-')
  const [lastRecorded, setLastRecorded] = react.useState('-')
  const [currentIwkbu, setCurrentIwkbu] = react.useState('-')
  const [currentPrice, setCurrentPrice] = react.useState(0)
  const [currentSdwkllj, setCurrentSdwkllj] = react.useState('-')
  const [currentReceipt, setCurrentReceipt] = react.useState('-')
  const [currentRecorded, setCurrentRecorded] = react.useState('-')

  react.useEffect(() => {
    init(parseInt(id || ""))
  }, [])

  const changedCurrentSdwkllj = (e:react.ChangeEvent<HTMLInputElement>) => {
    setCurrentSdwkllj(libDate.dateToIso( e.target.value))
  }
  // const changedRefGolongan = (e:react.ChangeEvent<HTMLSelectElement>) => {
  //   setRefGolongan(e.target.value)
  //   changedTarif({golongan: e.target.value})
  // }
  // const changedRefSeat = (e:react.ChangeEvent<HTMLInputElement>) => {
  //   setRefSeat(parseInt(e.target.value))
  //   changedTarif({seat: parseInt(e.target.value)})
  // }
  // const changedRefNoKps = (e:react.ChangeEvent<HTMLInputElement>) => {
  //   setRefNoKps(e.target.value)
  // }
  // const changedRefMasaLaku = (e:react.ChangeEvent<HTMLInputElement>) => {
  //   setRefMasaLaku(libDate.dateToIso(e.target.value, 'Asia/Jakarta'))
  // }
  // const startEdit = () => {
  //   setRefMasaLaku('')
  //   setRefIzin(izinList[0])
  //   setRefSource('KPS')
  //   setRefSeat(0)
  //   setRefNoKps('')
  //   setIsEdit(true)
  // }
  // const saveEdit = async () => {
  //   setIsEdit(false)
  //   const payload:kpsReqDataType = {
  //     kodeTarif: refKodeTarif,
  //     seat: refSeat,
  //     masaLaku: refMasaLaku,
  //     imageUrl: '',
  //     dasiIwkbuPenerimaanId: parseInt(id || '0'),
  //     noKps: refNoKps,
  //   }
  //   try {
  //     const fileData = imageFile as File
  //     const fileDestruct = fileData.name.split('.')
  //     const fileFormat = fileDestruct[fileDestruct.length - 1]
  //     const fileName = `${refNoKps.replaceAll('/', '-')}.${fileFormat}`
  //     const formData = new FormData()
  //     formData.append('kps', fileData, fileName)
  //     const resUp = await facKps.upKps(formData)
  //     payload.imageUrl = resUp.data
  //     const res = await facKps.postKps(payload)
  //     notification.notifySuccess(res.message)
  //     setIsEdit(false)
  //   } catch (err:any) {
  //     notification.notifyError(err.message)
  //   }
  // }
  const init = async (id:number) => {
    try {
      const res:responseType = await facPenerimaan.getDetailById(id)
      const resData:IwkbuDataType = res.data
      setNopol(resData.nopol)
      setLastSdwkllj(resData.lastSwdkllj)
      setLastIwkbu(resData.lastIwkbu)
      setLastPrice(resData.lastPrice)
      setLastReceipt(resData.lastReceipt)
      setLastRecorded(resData.lastRecorded)
      setCurrentSdwkllj(resData.currentSwdkllj)
      setCurrentIwkbu(resData.currentIwkbu)
      setCurrentPrice(resData.currentPrice)
      setCurrentReceipt(resData.currentReceipt)
      setCurrentRecorded(resData.currentRecorded)
    } catch (err:any) {
      notification.notifyError(err.message)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <div className="btn mb-60">
          <Link to="/" className="flex gap-x-8"><i className="aru-icon-arrow-left" /> Kembali</Link>
        </div>
        <section className="bg-white rounded-lg shadow grid grid-cols-3 gap-x-20 p-20 mb-20">
          <div>
            <p className="font-bold mb-24">Informasi Umum</p>
            <p>{nopol}</p>
          </div>
          <div className="">
            <p className="font-bold mb-24">Data Tahun Lalu</p>
            <div className="grid grid-cols-2 mb-24 gap-y-8">
              <p>SDWKLLJ</p>
              <p>: {lastSdwkllj}</p>
              <p>IWKBU</p>
              <p>: {lastIwkbu}</p>
              <p>Resi</p>
              <p>: {lastReceipt}</p>
              <p>Nominal</p>
              <p>: {helpFormatter.currency(lastPrice)}</p>
            </div>
          </div>
          <div className="">
            <p className="font-bold mb-24">Data Tahun Ini</p>
            <div className="grid grid-cols-2 mb-24 gap-y-8">
              <p>SDWKLLJ</p>
              <label>
                <p>: {currentSdwkllj}</p>
                <input hidden type="date" onChange={changedCurrentSdwkllj} />
              </label>
              <p>IWKBU</p>
              <p>: {currentIwkbu}</p>
              <p>Resi</p>
              <p>: {currentReceipt}</p>
              <p>Nominal</p>
              <p>: {helpFormatter.currency(currentPrice)}</p>
            </div>
            <div>
              {isEdit ? (
                <button onClick={() => {return}} className="btn btn-success">Save</button>
              ) : (
                <button onClick={() => {return}} className="btn btn-primary">Edit</button>
              )}
            </div>
          </div>
        </section>
        <SystemNotify />
        <WhatsNew />
      </main>
    </>
  )
}

export default ReportId
