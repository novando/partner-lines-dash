import react from 'react'
import WhatsNew from '@src/components/WhatsNew'
import Header from '@src/components/Header'
import notification from '@arutek/core-app/helpers/notification'
import facUser from '@src/factories/user'
import SystemNotify from '@src/components/SystemNotify'

const Settings = () => {
  const [dasiPass, setDasiPass] = react.useState('')
  const [dasiUser, setDasiUser] = react.useState('')

  const changeDasiPass = (e:react.ChangeEvent<HTMLInputElement>) => {
    setDasiPass(e.target.value)
  }
  const changeDasiUser = (e:react.ChangeEvent<HTMLInputElement>) => {
    setDasiUser(e.target.value)
  }
  const submitData = async() => {
    const payload = {dasiUser, dasiPass}
    try {
      await facUser.editSelf(payload)
      notification.notifySuccess('Data berhasil diupdate')
    } catch (err:any) {
      notification.notifyError(err.message)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto mb-60">
        <section className="bg-white rounded-lg shadow grid grid-cols-2 gap-x-20 p-20 mb-20">
          <div>
            {/* <p className="font-bold mb-24">Ubah Password</p>
            <div className="grid grid-cols-2">
              <p>Password</p>
              <div className="flex">
                <span>: </span>
                <input type="password" onChange={changePass} accept=".png,.jpeg,.jpg" />
              </div>
              <p>Konfirmasi Password</p>
              <div className="flex">
                <span>: </span>
                <input type="password" onChange={changeConf} accept=".png,.jpeg,.jpg" />
              </div>
            </div> */}
          </div>
          <div>
            <p className="font-bold mb-24">Ubah Data Admin</p>
            <p className="mb-16"> Data kredensial yang diinputkan di sini, akan digunakan untuk sinkronisasi data yang ada di DASI dan yang telah diinput pada Sip Aja. Sip Aja tidak akan mengubah data apapun yang ada di DASI.</p>
            <div className="grid grid-cols-2 gap-y-16">
              <p>Username</p>
              <div className="flex">
                <span>: </span>
                <input type="text" onChange={changeDasiUser} className="input" />
              </div>
              <p>Password</p>
              <div className="flex">
                <span>: </span>
                <input type="password" onChange={changeDasiPass} className="input" />
              </div>
              <div/>
              <button onClick={submitData} className="btn btn-primary">Update Data</button>
            </div>
          </div>
        </section>
        <WhatsNew />
      </main>
      <SystemNotify />
    </>
  )
}

export default Settings