import react from 'react'
import DataTable from '@arutek/core-app/components/DataTable'
import Header from '@src/components/Header'
import WhatsNew from '@src/components/WhatsNew'
import notification from '@arutek/core-app/helpers/notification'
import imgJr from '@arutek/core-app/images/jasaraharja/logo.png'
import facReport from '@factories/report'
import SystemNotify from '@src/components/SystemNotify'

const Dashboard = () => {
  const [isLoading, setIsLoading] = react.useState(false)
  const [filter, setFilter] = react.useState<armadaQueryType>({})
  const [total, setTotal] = react.useState(0)
  const [notMatch, setNotMatch] = react.useState(0)
  const [match, setMatch] = react.useState(0)
  const [overload, setOverload] = react.useState(0)
  const [dataEmpty, setDataEmpty] = react.useState(0)
  const [poList, setPoList] = react.useState<poResDataType[]>([])
  const [poTotal, setPoTotal] = react.useState(0)
  const [namaPo, setNamaPo] = react.useState('')
  const [kat, setKat] = react.useState(0)
  const [page, setPage] = react.useState(1)

  react.useEffect(() => {
    search(filter)
  }, [])
  
  const changedNamaPo = (e:react.ChangeEvent<HTMLInputElement>) => {
    setNamaPo(e.target.value)
    search({namaPo: e.target.value, page: 1})
  }
  const changedKat = (e:react.ChangeEvent<HTMLSelectElement>) => {
    setKat(parseInt(e.target.value))
    search({kat: parseInt(e.target.value), page: 1})
  }
  const changedPageSize = (e:react.ChangeEvent<HTMLSelectElement>) => {
    search({size: parseInt(e.target.value)})
  }
  const nextPage = () => {
    if (isLoading) return
    setPage(page + 1)
    search({page: page + 1})
  }
  const prevPage = () => {
    if (isLoading) return
    setPage(page - 1)
    search({page: page - 1})
  }
  const search = async (val:armadaQueryType) => {
    setIsLoading(true)
    const newFilter = Object.assign(filter, val)
    setFilter(newFilter)
    try {
      const summary = await facReport.getSummary(newFilter)
      const po = await facReport.getPo(newFilter)
      const summaryData:summaryResDataType = summary.data
      setTotal(summaryData.total)
      setNotMatch(summaryData.notMatch)
      setMatch(summaryData.match)
      setOverload(summaryData.overload)
      setDataEmpty(summaryData.empty)
      setPoList(po.data)
      setPoTotal(po.count as number)
    } catch (err:any) {
      notification.notifyError(err.toString())
    } finally {
      setIsLoading(false)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <section className="text-center bg-white rounded-lg shadow p-20 mb-20">
          <img className="mx-auto w-160 my-20" src={imgJr} alt="Jasaraharja" />
          <h1 className="typ-title-regular-bold mb-8">LINES</h1>
          <p className="typ-title-small-medium">Licensing Identification & Notifications Electronic System</p>
        </section>
        <section className="bg-white rounded-lg shadow p-20 mb-20">
          <div className="text-center flex items-center gap-20 mb-60">
            <div className="flex items-center gap-8">
              <p>Nama PO</p>
              <input 
                type="text"
                className="input"
                value={namaPo}
                placeholder='Cari Nama PO'
                onChange={changedNamaPo} />
            </div>
            <div className="flex items-center gap-8">
              <p>Kategori</p>
              <select value={kat} onChange={changedKat} className="input">
                <option value={0}>Semua</option>
                <option value={1}>1</option>
                <option value={2}>2</option>
                <option value={3}>3</option>
              </select>
            </div>
            <button onClick={() => search(filter)} className="btn btn-primary" type="button" disabled={isLoading}>Hitung</button>
          </div>
          <div>
            <p className="text-xl font-bold mb-20">Overview Data</p>
            <div className="flex justify-evenly mb-32">
              <div>
                <p className="text-center">Total Data</p>
                <p className="text-center text-7xl font-bold">{total}</p>
              </div>
            </div>
            <div className="flex justify-center gap-60 mb-60">
              <div>
                <p className="text-center">Jumlah Data Kosong</p>
                <p className="text-center text-5xl font-bold">{dataEmpty}</p>
              </div>
              <div>
                <p className="text-center">Jumlah Data Cocok</p>
                <p className="text-center text-5xl font-bold text-success">{match}</p>
              </div>
              <div>
                <p className="text-center">Jumlah Tarif Tidak Cocok</p>
                <p className="text-center text-5xl font-bold text-danger">{notMatch}</p>
              </div>
              <div>
                <p className="text-center">Jumlah Seat Tidak Cocok</p>
                <p className="text-center text-5xl font-bold text-yellow-90">{overload}</p>
              </div>
            </div>
          </div>
          <DataTable
            headComponent={
              <tr className="tr-head">
                <th className="py-8">No</th>
                <th className="py-8">Kode PO</th>
                <th className="py-8">Nama PO</th>
                <th className="text-center py-8">Kategori</th>
                <th className="py-8">Kantor</th>
                <th className="py-8">Data OK</th>
                <th className="py-8">Tarif Salah</th>
                <th className="py-8">Seat Salah</th>
                <th className="py-8">Kosong</th>
              </tr>
            }
            bodyComponent={poList && poList.map((res, key) => (
              <tr key={key} className="tr-body cursor-default">
                <td className="py-8">{key + 1}</td>
                <td className="py-8">{res.idPo}</td>
                <td className="py-8">{res.name}</td>
                <td className="text-center py-8">{res.kat}</td>
                <td className="py-8">{res.kantor}</td>
                <td className="py-8">{res.match}</td>
                <td className="py-8">{res.notMatch}</td>
                <td className="py-8">{res.overload}</td>
                <td className="py-8">{res.empty}</td>
              </tr>
            ))}
            totalData={poTotal}
            next={nextPage}
            prev={prevPage}
            size={changedPageSize}
            filter={filter} />
        </section>
        <SystemNotify />
        <WhatsNew />
      </main>
    </>
  )
}

export default Dashboard