import react from 'react'
import libDate from '@arutek/core-app/libraries/date'
import imgMaintenance from '@arutek/core-app/images/common/maintenance-logo.svg'

const Maintenance = () => {
  const endTime = 1689174000000
  const [duration, setDuration] = react.useState<Duration>()
  const [overtime, setOvertime] = react.useState(false)

  react.useEffect(() => {
    countdown()
  }, [])

  const countdown = () => {
    if (Date.now() > endTime) return setOvertime(true)
    setDuration(libDate.intervalToDuration({start: Date.now(), end: endTime}))
    setInterval(() => {
      countdown()
    }, 1000)
  }
  return (
    <>
      <main className="max-w-7xl mx-auto text-center">
        <section className="flex flex-col items-center mb-60">
          <img className="h-300 aspect-auto" src={imgMaintenance} alt="Maintenance" />
          <p className="font-bold text-4xl">We are terribly sorry</p>
          <p className="font-medium text-2xl">The sites is under maintenance to serve you better</p>
        </section>
        <section className="font-medium text-xl">
          <p className="mb-30">But hold on tight, we will be back in</p>
          {overtime ? (
            <section className="flex justify-evenly items-center">
              <div className="flex flex-col items-center">
                <h1 className="font-bold text-6xl text-primary mb-16">{duration?.hours}</h1>
                <p>{duration?.hours as number > 1 ? 'Hours' : 'Hour'}</p>
              </div>
              <div className="flex flex-col items-center">
                <h1 className="font-bold text-6xl text-primary mb-16">{duration?.minutes}</h1>
                <p>{duration?.minutes as number > 1 ? 'Minutes' : 'Minute'}</p>
              </div>
              <div className="flex flex-col items-center">
                <h1 className="font-bold text-6xl text-primary mb-16">{duration?.seconds}</h1>
                <p>{duration?.seconds as number > 1 ? 'Seconds' : 'Second'}</p>
              </div>
            </section>
          ) : (
            <h1 className="font-bold text-6xl text-primary mb-16">Any minute now</h1>
          )}
        </section>
      </main>
    </>
  )
}

export default Maintenance
