import react from 'react'
import { useParams, Link } from 'react-router-dom'
import WhatsNew from '@src/components/WhatsNew'
import Header from '@src/components/Header'
import notification from '@arutek/core-app/helpers/notification'
import libDate from '@arutek/core-app/libraries/date'
import facKps from '@factories/kps'
import SystemNotify from '@src/components/SystemNotify'

const Detail = () => {
  const { armadaId } = useParams()
  const [isEdit, setIsEdit] = react.useState(false)
  const [nopol, setNopol] = react.useState('')
  const [namaPo, setNamaPo] = react.useState('')
  const [filter, setFilter] = react.useState<tarifFilterType>({size:100})
  const [kat, setKat] = react.useState(0)
  const [refMasaLaku, setRefMasaLaku] = react.useState('0001-01-01')
  const [refSource, setRefSource] = react.useState('')
  const [refNoKps, setRefNoKps] = react.useState('')
  const [refImageUrl, setRefImageUrl] = react.useState('')
  const [refGolongan, setRefGolongan] = react.useState('')
  const [refIzin, setRefIzin] = react.useState('')
  const [refTarif, setRefTarif] = react.useState(0)
  const [refMaxSeat, setRefMaxSeat] = react.useState(0)
  const [refMinSeat, setRefMinSeat] = react.useState(0)
  const [refSeat, setRefSeat] = react.useState(1)
  const [refKodeTarif, setRefKodeTarif] = react.useState('')

  const [dasiGolongan, setDasiGolongan] = react.useState('')
  const [dasiIzin, setDasiIzin] = react.useState('')
  const [dasiTarif, setDasiTarif] = react.useState(0)
  const [dasiMaxSeat, setDasiMaxSeat] = react.useState(0)
  const [dasiMinSeat, setDasiMinSeat] = react.useState(0)
  const [dasiSeat, setDasiSeat] = react.useState(0)
  const [dasiKodeTarif, setDasiKodeTarif] = react.useState('')

  const [imageFile, setImageFile] = react.useState<File>()
  const [izinList, setIzinList] = react.useState<string[]>([])
  const [golonganList, setGolonganList] = react.useState<string[]>([])

  react.useEffect(() => {
    init(parseInt(armadaId || ""))
  }, [])

  const changedTarif = async (newFilter:tarifFilterType, isOnload = false) => {
    setFilter(Object.assign(filter, newFilter))
    try {
      const res = await facKps.getTarif(filter)
      const izins:string[] = []
      const golongans:string[] = []
      if (!res.data) res.data = []
      res.data.forEach((item:tarifType) => {
        if (!izins.includes(item.izin)) izins.push(item.izin)
        if (!golongans.includes(item.golongan)) golongans.push(item.golongan)
      })
      if (isOnload) {
        setIzinList(izins)
        return
      }
      setRefMaxSeat(res.data[0].maxSeat)
      setRefMinSeat(res.data[0].minSeat)
      setRefKodeTarif(res.data[0].kodeTarif)
      setRefTarif(res.data[0].tarif)
      setGolonganList(golongans)
    } catch (err:any) {
      notification.notifyError(err.message)
    }
  }
  const uploadFile = (e:react.ChangeEvent<HTMLInputElement>) => {
    const fileData = (e.target.files as FileList)[0]
    const fileReader = new FileReader()
    fileReader.readAsDataURL(fileData)
    fileReader.onload = () => {
      setRefImageUrl(fileReader.result as string)
    }
    setImageFile(fileData)
  }
  const changedRefIzin = (e:react.ChangeEvent<HTMLSelectElement>) => {
    setRefIzin(e.target.value)
    changedTarif({izin: e.target.value, golongan: '', seat: refSeat})
  }
  const changedRefGolongan = (e:react.ChangeEvent<HTMLSelectElement>) => {
    setRefGolongan(e.target.value)
    changedTarif({golongan: e.target.value})
  }
  const changedRefSeat = (e:react.ChangeEvent<HTMLInputElement>) => {
    setRefSeat(parseInt(e.target.value))
    changedTarif({seat: parseInt(e.target.value)})
  }
  const changedRefNoKps = (e:react.ChangeEvent<HTMLInputElement>) => {
    setRefNoKps(e.target.value)
  }
  const changedRefMasaLaku = (e:react.ChangeEvent<HTMLInputElement>) => {
    setRefMasaLaku(libDate.dateToIso(e.target.value, 'Asia/Jakarta'))
  }
  const startEdit = () => {
    setRefMasaLaku('')
    setRefIzin(izinList[0])
    setRefSource('KPS')
    setRefSeat(0)
    setRefNoKps('')
    setIsEdit(true)
  }
  const saveEdit = async () => {
    setIsEdit(false)
    const payload:kpsReqDataType = {
      kodeTarif: refKodeTarif,
      seat: refSeat,
      masaLaku: refMasaLaku,
      imageUrl: '',
      dasiIwkbuPenerimaanId: parseInt(armadaId || '0'),
      noKps: refNoKps,
    }
    try {
      const fileData = imageFile as File
      const fileDestruct = fileData.name.split('.')
      const fileFormat = fileDestruct[fileDestruct.length - 1]
      const fileName = `${refNoKps.replaceAll('/', '-')}.${fileFormat}`
      const formData = new FormData()
      formData.append('kps', fileData, fileName)
      const resUp = await facKps.upKps(formData)
      payload.imageUrl = resUp.data
      const res = await facKps.postKps(payload)
      notification.notifySuccess(res.message)
      setIsEdit(false)
    } catch (err:any) {
      notification.notifyError(err.message)
    }
  }
  const init = async (armadaId:number) => {
    try {
      const res:responseType = await facKps.getArmadaDetails(armadaId)
      const resData:armadaDetailsType = res.data
      setRefMasaLaku(resData.reference.masaLaku)
      setRefGolongan(resData.reference.golongan)
      setRefIzin(resData.reference.izin)
      setRefTarif(resData.reference.tarif)
      setRefSource(resData.reference.source)
      setRefMaxSeat(resData.reference.maxSeat)
      setRefMinSeat(resData.reference.minSeat)
      setRefSeat(resData.reference.seat > 0 ? resData.reference.seat : 1)
      setRefImageUrl(resData.reference.imageUrl)
      setRefKodeTarif(resData.reference.kodeTarif)
      setRefNoKps(resData.reference.noKps)
      setDasiGolongan(resData.dasi.golongan)
      setDasiIzin(resData.dasi.izin)
      setDasiTarif(resData.dasi.tarif)
      setDasiMaxSeat(resData.dasi.maxSeat)
      setDasiMinSeat(resData.dasi.minSeat)
      setDasiSeat(resData.dasi.seat)
      setDasiKodeTarif(resData.dasi.kodeTarif)
      setNopol(resData.nopol)
      setNamaPo(resData.poName)
      setKat(resData.poKat)
      changedTarif(filter, true)
    } catch (err:any) {
      notification.notifyError(err.message)
    }
  }
  return (
    <>
      <Header />
      <main className="max-w-7xl mx-auto">
        <div className="btn mb-60">
          <Link to="/" className="flex gap-x-8"><i className="aru-icon-arrow-left" /> Kembali</Link>
        </div>
        <section className="bg-white rounded-lg shadow grid grid-cols-3 gap-x-20 p-20 mb-20">
          <div>
            <p className="font-bold mb-24">Informasi Umum</p>
            <div className="grid grid-cols-2">
              <p>Nopol</p>
              <p>: {nopol}</p>
              <p>Nama PO</p>
              <p>: {namaPo}</p>
              <p>Kategori PO</p>
              <p>: {kat}</p>
              <img className="col-span-2 my-16" src={refImageUrl} alt={`Sumber ${refSource}`} />
            </div>
          </div>
          <div className="">
            <p className="font-bold mb-24">Data DASI</p>
            <div className="grid grid-cols-2">
              <p>Kode Tarif</p>
              <p>: {dasiKodeTarif}</p>
              <p>Izin</p>
              <p>: {dasiIzin}</p>
              <p>Golongan</p>
              <p>: {dasiGolongan}</p>
              <p>Seat Range</p>
              <p>: {dasiMinSeat} - {dasiMaxSeat > 1 ? dasiMaxSeat : "Tak terbatas"} </p>
              <p>Seat Saat ini</p>
              <p>: {dasiSeat}</p>
              <p>Tarif</p>
              <p>: {dasiTarif}</p>
            </div>
          </div>
          <div className="">
            <p className="font-bold mb-24">Data Referensi</p>
            <div className="grid grid-cols-2 mb-24 gap-y-8">
              <p>Sumber</p>
              <p>: {refSource}</p>
              {(refSource === 'KPS' || isEdit) && (
                <p>Nomor KPS</p>
              )}
              {(refSource === 'KPS' || isEdit) && (
                <div className="flex"><span>: </span><input type="text" className={isEdit ? `input-sm` : `input-sm bg-dark-grey-30 text-dark-grey-80`} value={refNoKps} onChange={changedRefNoKps} disabled={!isEdit} /></div>
              )}
              <p>Kode Tarif</p>
              <p>: {refKodeTarif}</p>
              <p>Izin</p>
              <div className="flex">
                <span>: </span>
                {isEdit ? (
                  <select className={isEdit ? `input-sm` : `input-sm bg-dark-grey-30 text-dark-grey-80`} value={refIzin} onChange={changedRefIzin} disabled={!isEdit}>
                    {izinList.map((item, key) => (
                      <option key={key} value={item}>{item}</option>
                    ))}
                  </select>
                ) : (<p>{refIzin}</p>)}
              </div>
              <p>Golongan</p>
              <div className="flex">
                <span>: </span>
                {isEdit ? (
                  <select className={isEdit ? `input-sm` : `input-sm bg-dark-grey-30 text-dark-grey-80`} value={refGolongan} onChange={changedRefGolongan}  disabled={!isEdit}>
                    {golonganList.map((item, key) => (
                      <option key={key} value={item}>{item}</option>
                    ))}
                  </select>
                ) : (<p>{refGolongan}</p>)}
              </div>
              <p>Seat Range</p>
              <p>: {refMinSeat} - {refMaxSeat > 1 ? refMaxSeat : "Tak terbatas"} </p>
              <p>Seat Saat ini</p>
              <div className="flex">
                <span>: </span>
                {isEdit ? (
                  <input type="number" className={isEdit ? `input-sm` : `input-sm bg-dark-grey-30 text-dark-grey-80`} min="1" value={refSeat} onChange={changedRefSeat} disabled={!isEdit} />
                ) : (<p>{refSeat}</p>)}
              </div>
              <p>Tarif</p>
              <p>: {refTarif}</p>
              <p>Masa Laku</p>
              <div className="flex">
                <span>: </span>
                {isEdit ? (
                  <input type="date" className={isEdit ? `input-sm` : `input-sm bg-dark-grey-30 text-dark-grey-80`} onChange={changedRefMasaLaku} disabled={!isEdit} />
                ) : (
                  <p>{libDate.isoToDate1(refMasaLaku, 'id')}</p>
                )}
              </div>
              {isEdit && <p>Upload KPS</p>}
              {isEdit && 
                <div className="flex">
                  <span>: </span>
                  <input type="file" onChange={uploadFile} accept=".png,.jpeg,.jpg" />
                </div>
              }
            </div>
            <div>
              {isEdit ? (
                <button onClick={saveEdit} className="btn btn-success">Save</button>
              ) : (
                <button onClick={startEdit} className="btn btn-primary">Edit</button>
              )}
            </div>
          </div>
        </section>
        <SystemNotify />
        <WhatsNew />
      </main>
    </>
  )
}

export default Detail
