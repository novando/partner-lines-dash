type exportCsvData = string[]|string[][]|number[]|number[][]
type armadaStatusType = 'MATCHED'|'SEAT_MISMATCH'|'NOT_MATCHED'
interface poNameType {
  query: string,
  suggestions: string[],
}
interface debugParamsType {
  documentURL: string,
  request: {
    postData?: string
  }
}
interface summaryArmadaType {
  namaPo: string,
  total: number,
  valid: number,
}
interface poType extends pksDto {
  idPo: number,
  name: string,
  kat: string,
  armada: number
  tanggalPks: number,
  tanggalAddendum: number,
}
interface kpsDto {
  poId: number,
  name: string,
  kat: string,
  nopol: string,
  izin?: izinType,
  golongan?: golonganType,
  seat?: number,
  tarif?: number,
  masaLaku?: string,
  kps?: string,
  spionamSs?: string,
}
interface pksDto {
  poId: number,
  pks: string,
  nomorPks: string,
  tanggalPks: string,
  addendum: string,
  nomorAddendum: string,
  tanggalAddendum: string,
}

interface routeType {
  path: string,
  name?: name,
  element: JSX.Element,
  needAuth?: bool,
}
interface loginType {
  credential: string,
  password: string,
}
interface registerType {
  username: string,
  password: string,
  passConf: string,
  roleId?: number,
}
interface armadaDetailsType {
  id: number,
  nopol: string,
  poKat: number,
  poName: string,
  dasi: dasiType,
  reference: referenceType,
}
interface tarifReqType {
  golongan: string,
  izin: string,
  kodeTarif: string,
  maxSeat: number,
  minSeat: number,
  tarif: number,
}
interface tarifType extends tarifReqType {
  seat: number,
}
interface responseType {
  data: any,
  message: string,
  count?: number,
}
interface armadaType {
  id: number,
  nopol: string,
  seat: string,
  poName: string,
  poKat: number,
  dasi: string,
  reference: string,
  status: armadaStatusType,
}
interface filterQueryType {
  page?: number,
  size?: number,
}
interface kodePoFilterType extends filterQueryType {
  kodePo?: string,
  namaPo?: string,
}
interface tarifFilterType extends filterQueryType {
  izin?: string,
  golongan?: string,
  seat?: number,
}
interface userFilterType extends filterQueryType {
  username?: string,
}
interface armadaQueryType extends filterQueryType {
  nopol?: string,
  namaPo?: string,
  kat?: number,
}
interface PenerimaanQueryType extends filterQueryType {
  till?: string,
  since?: string,
  nopol?: string,
}
interface reportQueryType {
  kantorId?: number,
  kat?: number,
  namaPo?: string,
}
interface kantorQueryType {
  name?: string,
}
interface kantorResDataType {
  id: number,
  name: string,
}
interface userKantorReqDataType {
  kantorId: number,
}
interface kpsReqDataType {
  noKps: string,
  masaLaku: string, // ISO Format
  seat: number,
  imageUrl: string,
  kodeTarif: string,
  dasiIwkbuPenerimaanId: number,
}
interface userResDataType {
  id: number,
  username: string,
  firstname: string,
  lastname: string,
  phone: string,
  email: string,
  roleId: number,
  kantorId: number,
}
interface roleResDataType {
  id: number,
  name: string,
}
interface summaryResDataType {
  total: number,
  match: number,
  notMatch: number,
  overload: number,
  empty: number,
}
interface poResDataType extends summaryResDataType {
  id: number,
  idPo: string,
  name: string,
  kat: number,
  kantor: string,
}
interface kodePoResTpe {
  idPo: string,
  name: string,
}
interface IwkbuDataType {
  id:	number
  note:	string
  nopol: string
  lastReceipt: string
  lastPrice: number
  lastRecorded: string
  lastIwkbu: string
  lastSwdkllj: string
  currentReceipt: string
  currentPrice: number
  currentRecorded: string
  currentIwkbu: string
  currentSwdkllj: string
}